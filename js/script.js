$(document).ready(function(){
	$('.owl-carousel').owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		// autoplay: true,
		// autoplayTimeout: 5000,
        // autoplaySpeed: 450,
		// autoplayHoverPause: false,
		navText: [
			'<div class="prev"></div>',
			'<div class="next"></div>'
		]
	});
});




// tabs portfolio nav
// $('.portfolio-navigation ul li a').on('click', function( e ) {
// 	e.preventDefault();
//
// 	var id = $(this).attr('href');
// 	$('.portfolio-container .active').removeClass('active');
// 	$(id).addClass('active');
// });



$('.header-menu ul li a').on('click', function( e ){
    e.preventDefault();

    var id = $(this).attr('href'); /*создаем переменную,
     обращаемся к тому элементу по которому мы кликнули.
      вытягиваем значение атрибута href.  */

    var top = $(id).offset().top; /*возвращаем обьект
     с параметрами расстрояния от верхнего края документа */

    $('html, body').animate({
        scrollTop: top
    }, 500);
});

$('.header-logo a').on('click', function( e ){
    e.preventDefault();

    var id = $(this).attr('href'); /*создаем переменную,
	 обращаемся к тому элементу по которому мы кликнули.
	 вытягиваем значение атрибута href.  */

    var top = $(id).offset().top; /*возвращаем обьект
	 с параметрами расстрояния от верхнего края документа */

    $('html, body').animate({
        scrollTop: top
    }, 500);
});


$(".block-inside-wrap-portfolio group").fancybox({
	// Options will go here
    speed : 330,
    loop : true,
    opacity : 'auto',

});
